const { get } = require("./storage");
const filter = { urls: ["<all_urls>"] };

// so on this they tried to obfuscate and make it more difficult to get rid
const extraInfo = ["blocking", "requestHeaders", "extraHeaders"];
const userAgent = "user-agent";

/**
 * Allow only headers without user agent
 * @method
 * @param {string} options.name
 * @returns {boolean}
 */
const filterUserAgent = ({ name }) =>
  name.toLowerCase().includes(userAgent) === false;

function before_headers(req) {
  if (get("enabled") === true) {
    // the user is allowing to send it as-is
    return;
  }

  // the user has opted not to send the user agent
  const requestHeaders = req.requestHeaders.filter(filterUserAgent);
  return { requestHeaders };
}

chrome.webRequest.onBeforeSendHeaders.addListener(
  before_headers,
  filter,
  extraInfo
);
