# webextension-user-agent

Feature:

+ Toggle on or off sending `User-Agent:` header

It's safe. You can view the source which is concise. You can build it yourself by following the development and build instructions. It's also good if you just want a base to build your own extension on.

## Table of Contents

+ [Development](#development)
+ [Build](#build)
+ [Todo](#todo)
+ [Copying, license, and contributing](#copying-license-and-contributing)

## Development

```sh
npm run dev
```

## Build

```sh
npm run prd
```

## Todo

Load `./dist` into your browser.

For the future

+ [Pack extension for Chrome](https://www.adambarth.com/experimental/crx/docs/packaging.html). Zip and sign the extension for distribution.

## Copying, license, and contributing

Copyright (C) Tony Crowe 2020 <https://tonycrowe.com/contact/>

Thank you for using and contributing to make webextension-user-agent better.

⚠️ Please run `npm run prd` before submitting a patch.

⚖️ webextension-user-agent is free and unlicensed. Anyone can use it, fork, or modify and we the community will try to help whenever possible.
